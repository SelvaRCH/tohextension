import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Spider Man', color: 'red' },
    { id: 12, name: 'Iron Man', color: 'goldenrod'  },
    { id: 13, name: 'Hulk', color: 'green'  },
    { id: 14, name: 'Flash', color: 'red'  },
    { id: 15, name: 'Captain America', color: 'blue'  },
    { id: 16, name: 'Super man', color: 'blue'  },
    { id: 17, name: 'PowerPuff1', color: 'goldenrod'  },
    { id: 18, name: 'Dr Strange', color: 'blue'  },
    { id: 19, name: 'Wonder Woman', color: 'brown'  },
    { id: 20, name: 'Batman', color: 'black'  }
];
