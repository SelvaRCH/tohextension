import { Component, OnInit } from '@angular/core';
import {HeroService} from '../../hero.service';

@Component({
  selector: 'add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.css']
})
export class AddHeroComponent implements OnInit {

  addHeroFormEnabled = false;
  colors = ['red', 'blue', 'green', 'brown', 'black', 'goldenrod'];
  model = {
    name: '',
    color: ''
  };

  constructor(private heroService: HeroService) {

  }

  ngOnInit() {
  }

  toggleAddHeroFormVisibility() {
    this.addHeroFormEnabled = !this.addHeroFormEnabled;
  }

  onSubmit() {
    this.heroService.addHeroSubject.next(this.model);
    this.toggleAddHeroFormVisibility();
    this.model = {
      name: '',
      color: ''
    };
  }

}
