import { Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[appHeroColor]'
})
export class HighlightDirective {

  @Input('appHeroColor') highlightColor: string;

  constructor(private el: ElementRef) {
    setTimeout (() => {
      this.el.nativeElement.style.backgroundColor = this.highlightColor;
    }, 0);
  }
}
